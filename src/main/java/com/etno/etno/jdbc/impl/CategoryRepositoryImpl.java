package com.etno.etno.jdbc.impl;

import com.etno.etno.jdbc.repository.CategoryRepository;
import com.etno.etno.model.Category;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private CategoryRowMapper categoryRowMapper;

    public CategoryRepositoryImpl(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate, CategoryRowMapper categoryRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.categoryRowMapper = categoryRowMapper;
    }

    @Override
    public List<Category> getCategoryList() {
        String sql = "select * from \"Category\"";

        return jdbcTemplate.query(sql, categoryRowMapper);

    }

    @Override
    public void addCategory(Category category) {
        String sql = "insert into \"Category\"(\"ParentId\", \"Name\")\n" +
                " values (:category_parentId, :category_name)";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource("category_name", category.getName());
    //    parameterSource.addValue("id", category.getId());
        parameterSource.addValue("category_parentId", category.getParentId() == 0 ? null :  category.getParentId());

        int saved = namedParameterJdbcTemplate.update(sql, parameterSource);
        if (saved > 0){
            System.out.println("Category created");
        }
        if(saved < 0){
            System.out.println("Category was not created");
        }
    }

    @Override
    public Optional<Category> getCategoryById(long id) {
        Optional<Category> optionalCategory = Optional.empty();
        String sql = "select * from \"Category\"\n" +
                "\twhere ID=:category_id";

        MapSqlParameterSource params =  new MapSqlParameterSource("category_id", id);
        List<Category> categoryList = namedParameterJdbcTemplate.query(sql, params, categoryRowMapper);
        if (!categoryList.isEmpty()){
            optionalCategory = Optional.of(categoryList.get(0));
        }
        return optionalCategory;
    }

    @Override
    public void updateCategory(long id, Category category) {
        String sql = "UPDATE \"Category\"\n" +
                "\tSET  \"Name\"=:category_name, \"ParentId\"=:category_parentId\n" +
                "\tWHERE ID=:category_id" ;


        MapSqlParameterSource parameterSource = new MapSqlParameterSource("category_id", id);
       // parameterSource.addValue("new_category_id", category.getId());
        parameterSource.addValue("category_name", category.getName());
        parameterSource.addValue("category_parentId",  category.getParentId() == 0 ? null :  category.getParentId());
        int updated = namedParameterJdbcTemplate.update(sql, parameterSource);
        if (updated>0){
            System.out.println("Category was updated");
        }else{
            System.out.println("Category was not updated");
        }

    }
    @Override
    public void deleteCategory(long id) {
        String sql = "DELETE FROM \"Category\"\n" +
                "\tWHERE ID=:category_id";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource("category_id", id);
        int deleted = namedParameterJdbcTemplate.update(sql,parameterSource);
        if (deleted>0){
            System.out.println("Category was deleted");
        }else{
            System.out.println("Category was not deleted");
        }
    }
}