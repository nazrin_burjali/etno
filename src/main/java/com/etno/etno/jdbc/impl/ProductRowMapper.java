package com.etno.etno.jdbc.impl;

import com.etno.etno.model.Product;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ProductRowMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(ResultSet rs, int i) throws SQLException {
        Product product = new Product();
        product.setId(rs.getInt("id"));
        product.setProductName(rs.getString("productName"));
        product.setCategoryId(rs.getLong("categoryId"));
        product.setDescription(rs.getString("description"));
        product.setRentPrice(rs.getBigDecimal("rentPrice"));
        product.setSellPrice(rs.getBigDecimal("sellPrice"));
        return product;

    }
}