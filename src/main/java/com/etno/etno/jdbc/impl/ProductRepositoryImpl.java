package com.etno.etno.jdbc.impl;

import com.etno.etno.jdbc.repository.ProductRepository;
import com.etno.etno.model.Category;
import com.etno.etno.model.Product;
import com.etno.etno.validator.ProductValidator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@Repository
public class ProductRepositoryImpl implements ProductRepository {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private ProductRowMapper productRowMapper;

    public ProductRepositoryImpl(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate, ProductRowMapper productRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.productRowMapper = productRowMapper;
    }


    @Override
    public List<Product> getProductList() {
        String sql = "select * from \"Product\"";
        return jdbcTemplate.query(sql, productRowMapper);
    }

    @Override
    public void addProduct(Product product) {
        String sql = "insert into \"Product\"(\"CategoryID\", \"ProductName\", \"SellPrice\", \"RentPrice\", \"Description\", \"CreationDate\") " +
                " values (:category_id, :product_name, :sell_price, :rent_price, :description, :creationDate )";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource("product_name", product.getProductName());
        parameterSource.addValue("category_id", product.getCategoryId() == 0 ? null : product.getCategoryId());
        parameterSource.addValue("sell_price", product.getSellPrice().equals(BigDecimal.ZERO) ? null : product.getSellPrice());
        parameterSource.addValue("rent_price", product.getRentPrice().equals(BigDecimal.ZERO) ? null : product.getRentPrice());
        parameterSource.addValue("description", product.getDescription());
        parameterSource.addValue("creationDate", LocalDateTime.now());
        int saved = namedParameterJdbcTemplate.update(sql, parameterSource);
        if (saved > 0) {
            System.out.println("Product created");
        }
        if (saved < 0) {
            System.out.println("Product was not created");
        }
    }

    @Override
    public Optional<Product> getProductById(long id) {
        Optional<Product> optionalProduct = Optional.empty();
        String sql = "select * from \"Product\"\n" +
                "\twhere Id=:product_id";

        MapSqlParameterSource params = new MapSqlParameterSource("product_id", id);
        List<Product> products = namedParameterJdbcTemplate.query(sql, params, productRowMapper);
        if (products != null && !products.isEmpty()) {
            optionalProduct = Optional.of(products.get(0));
        }
        return optionalProduct;
    }


    @Override
    public void updateProduct(long id, Product product) {
        String sql = "UPDATE \"Product\"\n" +
                "\tSET  \"ProductName\"=:product_name, \"CategoryID\"= :category_id, \"SellPrice\"=:sell_price, \"RentPrice\"=:rent_price, \"Description\"=:description\n" +
                "\tWHERE Id=:product_id";


        MapSqlParameterSource parameterSource = new MapSqlParameterSource("product_id", id);
        parameterSource.addValue("product_name", product.getProductName());
        parameterSource.addValue("category_id", product.getCategoryId());
        parameterSource.addValue("sell_price", product.getSellPrice().equals(BigDecimal.ZERO) ? null : product.getSellPrice());
        parameterSource.addValue("rent_price", product.getRentPrice().equals(BigDecimal.ZERO) ? null : product.getRentPrice());
        parameterSource.addValue("description", product.getDescription());
        int updated = namedParameterJdbcTemplate.update(sql, parameterSource);
        if (updated > 0) {
            System.out.println("Product was updated");
        } else {
            System.out.println("Product was not updated");
        }

    }

    @Override
    public void deleteProduct(long id) {
        String sql = "DELETE FROM \"Product\"\n" +
                "\tWHERE Id=:product_id";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource("product_id", id);
        int deleted = namedParameterJdbcTemplate.update(sql, parameterSource);
        if (deleted > 0) {
            System.out.println("Product was deleted");
        } else {
            System.out.println("Product was not deleted");
        }
    }
}
