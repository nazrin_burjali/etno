package com.etno.etno.jdbc.repository;

import com.etno.etno.model.Category;
import com.etno.etno.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {
    List<Product> getProductList();
    void addProduct(Product product);
    Optional<Product> getProductById(long id);
    void updateProduct(long id, Product product);
    void deleteProduct(long id);
}
