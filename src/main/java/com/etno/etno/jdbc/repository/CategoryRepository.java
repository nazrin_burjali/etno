package com.etno.etno.jdbc.repository;

import com.etno.etno.model.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository {
    List<Category> getCategoryList();
    void addCategory(Category category);
    Optional<Category> getCategoryById(long id);
    void updateCategory(long id, Category category);
    void deleteCategory(long id);
}
