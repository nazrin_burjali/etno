package com.etno.etno.controller;

import com.etno.etno.dto.CategoryDto;

import com.etno.etno.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("/service/category")

public class CategoryController {


    @Autowired
    private CategoryService categoryService;

    @GetMapping("/")
    public List<CategoryDto> getAll() {
        return categoryService.getAll();
    }

    @GetMapping("/{id}")
    public CategoryDto getCategoryById(@PathVariable(name = "id") int categoryId) {
        CategoryDto categoryDto = categoryService.getCategoryById(categoryId);
        if (categoryDto == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Category id = " + categoryId + " id not found");
        }
        return categoryDto;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/")
    public void addCategory(@RequestBody CategoryDto categoryDto) {
        categoryService.addCategory(categoryDto);
    }

    @PutMapping("/{id}")
    public void updateCategory(@PathVariable(name = "id") int categoryId, @RequestBody CategoryDto categoryDto) {
        categoryService.updatedCategory(categoryId, categoryDto);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteCategory(@PathVariable(name = "id") long categoryId) {
        categoryService.deleteCategory(categoryId);
    }
}
