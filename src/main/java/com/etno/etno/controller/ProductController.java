package com.etno.etno.controller;


import com.etno.etno.dto.ProductDto;
import com.etno.etno.reponse.EtnoException;
import com.etno.etno.service.ProductService;
import com.etno.etno.validator.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductValidator productValidator;

    @Autowired
    private ProductService productService;


    @GetMapping("/")
    public List<ProductDto> getAll(){
        return productService.getAll();
    }

    @GetMapping("/{id}")
    public ProductDto getProductById(@PathVariable(name = "id") int productId){
        ProductDto productDto = productService.getProductById(productId);
        if (productDto == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Product id = " + productId + " id not found");
        }
        return  productDto;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/")
    public void addProduct(@RequestBody ProductDto productDto) throws EtnoException {
        productValidator.validate(productDto);
        productService.addProduct(productDto);
    }

    @PutMapping("/{id}")
    public void updateProduct(@PathVariable(name = "id") int productId, @RequestBody ProductDto productDto) throws EtnoException {
        productValidator.validate(productDto);
        productService.updatedProduct(productId, productDto);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable(name = "id") long productId){
        productService.deleteProduct(productId);
    }
}
