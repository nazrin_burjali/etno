package com.etno.etno.dto;

import java.math.BigDecimal;

public class ProductDto {
    private int id;
    private String productName;
    private BigDecimal rentPrice;
    private BigDecimal sellPrice;
    private String description;
    private Long categoryId;

    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getRentPrice() {
        return rentPrice;
    }

    public void setRentPrice(BigDecimal rentPrice) {
        this.rentPrice = rentPrice;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", rentPrice=" + rentPrice +
                ", sellPrice=" + sellPrice +
                ", description='" + description + '\'' +
                ", categoryId=" + categoryId +
                '}';
    }

}
