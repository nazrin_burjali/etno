package com.etno.etno.model;
import java.math.BigDecimal;


public class Product {

    private int id;
    private String productName;
    private BigDecimal rentPrice;
    private BigDecimal sellPrice;
    private String description;
    private Long categoryId;

    public Product() {
    }


    private Product(Builder builder) {
        this.id = builder.id;
        this.productName = builder.productName;
        this.rentPrice = builder.rentPrice;
        this.sellPrice = builder.sellPrice;
        this.description = builder.description;
        this.categoryId = builder.categoryId;

    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", rentPrice=" + rentPrice +
                ", sellPrice=" + sellPrice +
                ", description='" + description + '\'' +
                ", categoryId=" + categoryId +
                '}';
    }


    public static class Builder {
        private int id;
        private String productName;
        private BigDecimal rentPrice;
        private BigDecimal sellPrice;
        private String description;
        private Long categoryId;

        public Builder() {
        }

        public Builder(Product product) {
            this.id = product.id;
            this.productName = product.productName;
            this.rentPrice = product.rentPrice;
            this.sellPrice = product.sellPrice;
            this.description = product.description;
            this.categoryId = product.categoryId;
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withProductName(String productName) {
            this.productName = productName;
            return this;
        }

        public Builder withRentPrice(BigDecimal rentPrice) {
            this.rentPrice = rentPrice;
            return this;
        }

        public Builder withSellPrice(BigDecimal sellPrice) {
            this.sellPrice = sellPrice;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withCategoryId(Long categoryId) {
            this.categoryId = categoryId;
            return this;
        }

        public Product build() {
            return new Product(this);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getRentPrice() {
        return rentPrice;
    }

    public void setRentPrice(BigDecimal rentPrice) {
        this.rentPrice = rentPrice;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

//    public static Product convertFromJson(JsonObject json) {
//        return new Product.Builder()
//                .withId(Common.isEmpty(json.get("id")) ? 0 : json.get("id").getAsInt())
//                .withCategoryId(Common.isEmpty(json.get("categoryId")) ? null : json.get("categoryId").getAsLong())
//                .withProductName(Common.isEmpty(json.get("productName")) ? null : json.get("productName").getAsString())
//                .withRentPrice(Common.isEmpty(json.get("rentPrice")) ? null : json.get("rentPrice").getAsBigDecimal())
//                .withRentPrice(Common.isEmpty(json.get("sellPrice")) ? null : json.get("sellPrice").getAsBigDecimal())
//                .withDescription(Common.isEmpty(json.get("description")) ? null : json.get("description").getAsString())
//                .build();

    //   }
}
