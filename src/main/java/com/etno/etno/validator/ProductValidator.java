package com.etno.etno.validator;

import com.etno.etno.dto.ProductDto;
import com.etno.etno.model.Product;
import com.etno.etno.reponse.Errors;
import com.etno.etno.reponse.EtnoException;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.xml.bind.ValidationException;
import java.math.BigDecimal;

@Component
public class ProductValidator {

    public void validate(ProductDto productDto) throws EtnoException {

//        Product product = Product.convertFromJson(jsonObject);


        if (StringUtils.isEmpty(productDto.getProductName()))
            throw new EtnoException(Errors.errValidation, new ValidationException("Xonçanın adını daxil edin"));

        if (productDto.getCategoryId() == null)
            throw new EtnoException(Errors.errValidation, new ValidationException("Xonçanın aid olduğu kateqoriyanı daxil edin"));

        if (productDto.getSellPrice() == null)
            throw new EtnoException(Errors.errValidation, new ValidationException("Satış qiymətini daxil edin"));

        if (productDto.getSellPrice().equals(BigDecimal.ZERO) || productDto.getSellPrice().compareTo(BigDecimal.ZERO) < 0)
            throw new EtnoException(Errors.errValidation, new ValidationException("Satış qiyməti doğru daxil edilməyib"));

    }
}
