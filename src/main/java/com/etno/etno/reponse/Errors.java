package com.etno.etno.reponse;


import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Errors {
    /* ----------------------- Response Messages EN------------------------ */
    public static HashMap<String, String> responseMessagesEn = new HashMap<>();

    /* ----------------------- Response Messages Az------------------------ */
    public static HashMap<String, String> responseMessagesAz = new HashMap<>();



    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="BSE ERRORS">
    /* --------------- REJECTION CODES ------------ */
    public static final String noErr = "0000";
    public static final String productNameErr = "0001"; // product
    public static final String categoryErr = "0002"; // category
    //public static final String rentPriceErr = "0003"; // rent
    public static final String sellPriceErr = "0004"; // sell
    public static final String nullValueError = "2042";
    public static final String errValidation = "9995";



    static {
        responseMessagesEn.put(productNameErr, "0001 -Product name not found");
        responseMessagesEn.put(categoryErr, "0002 - Category was not found.");
       // responseMessagesEn.put(rentPriceErr, "0003 - Rent price is not");
        responseMessagesEn.put(sellPriceErr, "0004 - Sell price cannot be empty");
        responseMessagesEn.put(nullValueError, "Null Pointer Exception");
        responseMessagesEn.put(errValidation, "Validation ERROR");

    }


    public static String getResponseMessageEn(String key) {
        return responseMessagesEn.get(key);
    }

    static {
        responseMessagesAz.put(noErr, "Məlumatlar uğurla başa çatdı");
        responseMessagesAz.put(productNameErr, "Məhsul adı doğru deyil");
        responseMessagesAz.put(categoryErr, "Kateqoriya formata uyğun deyil");
        responseMessagesAz.put(sellPriceErr, "Satış qiyməti doğru deyil");
        responseMessagesAz.put(nullValueError, "Null dəyər istifadəsi");


    }

    public static String getResponseMessageAz(String key) {
        return responseMessagesAz.get(key);
    }
    //</editor-fold>


//
//    public static void handleException(Exception ex, Logger logger, Object... params) {
//        if (ex instanceof EtnoException) {
//            logger.error(((EtnoException) ex).getLogMessage(), ex);
//            System.out.println(((EtnoException) ex).getLogMessage());
//        } else {
//            logger.error(ex.getMessage(), ex);
//            System.out.println(ex.getMessage());
//        }
//        Arrays.stream(params).forEach(obj -> logger.error(obj.toString()));
//        Common.sendMailToCsdr(ex.getMessage(), params);
//
//    }
}