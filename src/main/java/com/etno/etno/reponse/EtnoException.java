package com.etno.etno.reponse;

import org.apache.commons.lang3.StringUtils;

public class EtnoException extends Exception{
    private String code;
    private String missingParameter;
    private boolean isSystem;


    public EtnoException(String code) {
        super(Errors.getResponseMessageEn(code));
        this.code = code;
    }

    public EtnoException(String code, Exception ex) {
        super(ex.getMessage());
        this.code = code;
    }

    public EtnoException(String code, boolean isSystem) {
        super(Errors.getResponseMessageEn(code));
        this.code = code;
        this.isSystem = isSystem;
    }

    public EtnoException(String code, String missingParameter) {
        super(Errors.getResponseMessageEn(code) + (StringUtils.isEmpty(missingParameter) ? "" : " " + missingParameter));
        this.code = code;
        this.missingParameter = missingParameter;
    }

    public EtnoException(String code, String missingParameter, boolean isSystem) {
        super(Errors.getResponseMessageEn(code) + (StringUtils.isEmpty(missingParameter) ? "" : " " + missingParameter));
        this.code = code;
        this.missingParameter = missingParameter;
        this.isSystem = isSystem;
    }

    public EtnoException(EtnoException csdr, boolean isSystem) {
        super(Errors.getResponseMessageEn(csdr.getCode()));
        this.code = csdr.code;
        this.missingParameter = csdr.missingParameter;
        this.isSystem = isSystem;
    }

    public static <T> T requireNonNull(T obj, String link) throws EtnoException {
        if (obj == null)
            throw new EtnoException(Errors.nullValueError, link);
        return obj;
    }

    public static EtnoException getException(String errorCode) {
        return new EtnoException(errorCode, Errors.getResponseMessageAz(errorCode));
    }

    public String getCode() {
        return code;
    }

    public String getMissingParameter() {
        return missingParameter;
    }

    public String getLogMessage() {
        return this.getCode() + ": " + Errors.getResponseMessageEn(code) + " - " + this.getMissingParameter();
    }

    public boolean isSystem() {
        return isSystem;
    }
}
