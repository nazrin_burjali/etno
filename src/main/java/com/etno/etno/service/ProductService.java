package com.etno.etno.service;


import com.etno.etno.dto.ProductDto;
import com.etno.etno.model.Product;

import java.util.List;

public interface ProductService {

    List<ProductDto> getAll();

    ProductDto getProductById(long id);

    void addProduct(ProductDto product);

    void updatedProduct(long id, ProductDto product);

    void deleteProduct(long id);
}
