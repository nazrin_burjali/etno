package com.etno.etno.service;



import com.etno.etno.dto.CategoryDto;

import java.util.List;

public interface CategoryService {

    List<CategoryDto> getAll();

    CategoryDto getCategoryById(long id);

    void addCategory(CategoryDto categoryDto);

    void updatedCategory(long id, CategoryDto categoryDto);

    void deleteCategory(long id);

}