package com.etno.etno.service;


import com.etno.etno.dto.CategoryDto;
import com.etno.etno.jdbc.repository.CategoryRepository;
import com.etno.etno.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private  CategoryRepository categoryRepository;



    private static CategoryDto convertEntityToDto(Category category) {
        CategoryDto dto = new CategoryDto();
        dto.setId(category.getId());
        dto.setName(category.getName());
        dto.setParentId(category.getParentId());
        return dto;
    }

    @Transactional
    @Override
    public void addCategory(CategoryDto categoryDto) {
        Category category = new Category();
        category.setId(categoryDto.getId());
        category.setName(categoryDto.getName());
        category.setParentId(categoryDto.getParentId());
        categoryRepository.addCategory(category);
    }

    @Override
    public void updatedCategory(long id, CategoryDto categoryDto) {
        Optional<Category> optionalCategory = categoryRepository.getCategoryById(id);
        if (optionalCategory.isPresent()) {
            Category category = new Category();
            category.setId(categoryDto.getId());
            category.setName(categoryDto.getName());
            category.setParentId(categoryDto.getParentId());
            categoryRepository.updateCategory(id, category);
        } else {
            throw new RuntimeException("Category id  = " + id + " id was not found");
        }
    }

    @Override
    public void deleteCategory(long id) {
        Optional<Category> optionalCategory = categoryRepository.getCategoryById(id);
        if (optionalCategory.isPresent()) {
            categoryRepository.deleteCategory(id);
        } else {
            throw new RuntimeException("Category id  = " + id + " id was not found");
        }
    }


    @Override
    public List<CategoryDto> getAll() {
        List<CategoryDto> categoryDtoList = categoryRepository.getCategoryList()
                .stream()
                .map(CategoryServiceImpl::convertEntityToDto)
                .collect(Collectors.toList());
        return categoryDtoList;
    }

    @Override
    public CategoryDto getCategoryById(long id) {
        Optional<Category> optionalCategory = categoryRepository.getCategoryById(id);
        CategoryDto categoryDto = null;
        if (optionalCategory.isPresent()) {
            categoryDto = convertEntityToDto(optionalCategory.get());
        }
        return categoryDto;
    }

}
