package com.etno.etno.service;

import com.etno.etno.dto.CategoryDto;
import com.etno.etno.dto.ProductDto;
import com.etno.etno.jdbc.repository.CategoryRepository;
import com.etno.etno.jdbc.repository.ProductRepository;
import com.etno.etno.model.Category;
import com.etno.etno.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    private static ProductDto convertEntityToDto(Product product) {
        ProductDto dto = new ProductDto();
        dto.setId(product.getId());
        dto.setCategoryId(product.getCategoryId());
        dto.setRentPrice(product.getRentPrice());
        dto.setDescription(product.getDescription());
        dto.setSellPrice(product.getSellPrice());
        dto.setProductName(product.getProductName());
        return dto;
    }

    @Transactional
    @Override
    public void addProduct(ProductDto productDto) {
        Product product = new Product();
        product.setId(productDto.getId());
        product.setCategoryId(productDto.getCategoryId());
        product.setSellPrice(productDto.getSellPrice());
        product.setRentPrice(productDto.getRentPrice());
        product.setProductName(productDto.getProductName());
        product.setDescription(productDto.getDescription());
        productRepository.addProduct(product);
    }

    @Override
    public List<ProductDto> getAll() {
        return productRepository.getProductList()
                .stream()
                .map(ProductServiceImpl::convertEntityToDto)
                .collect(Collectors.toList());
        }


    @Override
    public ProductDto getProductById(long id) {
        Optional<Product> optionalProduct = productRepository.getProductById(id);
        ProductDto productDto = null;
        if (optionalProduct.isPresent()) {
            productDto = convertEntityToDto(optionalProduct.get());
        }
        return productDto;
    }




    @Override
    public void updatedProduct(long id, ProductDto productDto) {
        Optional<Product> optionalProduct = productRepository.getProductById(id);
        if (optionalProduct.isPresent()) {
            Product product1  = new Product();
         //   product1.setId(productDto.getId());
            product1.setCategoryId(productDto.getCategoryId());
            product1.setDescription(productDto.getDescription());
            product1.setProductName(productDto.getProductName());
            product1.setRentPrice(productDto.getRentPrice());
            product1.setSellPrice(productDto.getSellPrice());
            productRepository.updateProduct(id, product1);
        } else {
            throw new RuntimeException("Product id  = " + id + " id was not found");
        }
    }

    @Override
    public void deleteProduct(long id) {
        Optional<Product> optionalProduct = productRepository.getProductById(id);
        if (optionalProduct.isPresent()) {
            productRepository.deleteProduct(id);
        } else {
            throw new RuntimeException("Product id  = " + id + " id was not found");
        }
    }
}
