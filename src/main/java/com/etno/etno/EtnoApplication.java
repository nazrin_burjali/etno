package com.etno.etno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EtnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EtnoApplication.class, args);
	}

}
