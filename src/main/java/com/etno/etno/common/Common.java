package com.etno.etno.common;

import com.google.gson.JsonElement;

public class Common {

    public static boolean isEmpty(JsonElement element) {
        return element == null || element.isJsonNull();
    }
}
